const express = require('express');
const router = express.Router();
const multer = require('multer');
// We can use s3 here for storing the uploaded csv file
const upload = multer({ dest: 'tmp/csv/' });

const uploadService = require('./uploadService/uploadService');
const employeeService = require('./employeeService/employeeService');

router.post('/upload-csv', upload.single('file'), uploadService.uploadCsv);
router.get('/employees', employeeService.getEmployeeList);

module.exports = router;
