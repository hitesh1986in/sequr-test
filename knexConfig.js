// Update with your config settings.
const dotenv = require('dotenv');
const env = process.env.NODE_ENV || 'development';
dotenv.config({ path: env + '.env' });

module.exports = {
    development: {
        client: 'postgresql',
        connection: {
            host: process.env.dbHost,
            database: process.env.dbName,
            user: process.env.dbUserName,
            password: process.env.dbPassword,
            charset: 'utf8'
        },
        pool: {
            min: 2,
            max: 10
        },
        migrations: {
            tableName: 'knex_migrations'
        }
    },

    staging: {
        client: 'postgresql',
        connection: {
            database: process.env.dbName,
            user: process.env.dbUserName,
            password: process.env.dbPassword
        },
        pool: {
            min: 2,
            max: 10
        },
        migrations: {
            tableName: 'knex_migrations'
        }
    },

    production: {
        client: 'postgresql',
        connection: {
            database: process.env.dbName,
            user: process.env.dbUserName,
            password: process.env.dbPassword
        },
        pool: {
            min: 2,
            max: 10
        },
        migrations: {
            tableName: 'knex_migrations'
        }
    }
};
