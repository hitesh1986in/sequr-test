const fs = require('fs');
const csv = require('fast-csv');

const KafkaService = require('../kafkaService/kafkaProducerService')

class UploadService {

    static uploadCsv(req, res) {
        fs.createReadStream(req.file.path)
            .pipe(csv.parse())
            .on('error', error => {
                console.error(error);
                res.sendStatus(500);
            })
            .on('data', (row) => {
                //console.log(`ROW=${JSON.stringify(row)}`)
                KafkaService.putMessage(row);
            })
            .on('end', (rowCount) => {
                console.log(`Parsed ${rowCount} rows`);
                fs.unlinkSync(req.file.path);
                res.sendStatus(200);
            });
    }
}

module.exports = UploadService;
