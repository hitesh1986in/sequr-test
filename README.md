<h3 align="center">sequr-assignment</h3>

---

<p align="center"> This is the small assignment on Kafka to learn the message broaking services
    <br> 
</p>

## 📝 Table of Contents

- [About](#about)
- [Getting Started](#getting_started)
- [Deployment](#deployment)
- [Usage](#usage)
- [Built Using](#built_using)
- [Authors](#authors)

## 🧐 About <a name = "about"></a>

This codebase helps you to process csv row as Kafka mesages i.e producing and consuming

## 🏁 Getting Started <a name = "getting_started"></a>

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See [deployment](#deployment) for notes on how to deploy the project on a live system.

### Prerequisites

```
node js
kafka service
postgreSQL
```

### Installing

A step by step series of examples that tell you how to get a development env running.

Say what the step will be

```
Copy the development file to development.env and add your database and kakfa configuration at developmetn.env
```

then

```
npm install
```

then

```
npm start
```

Now you can consume api service at localhost:9000/api

## 🎈 Usage <a name="usage"></a>

Since this is small project only 2 API so I not integrated the swagger document, however here are the API document as below

### /upload-csv
This api is post type REST call and accept one input as `file` where you can send the csv file. This will generate messages on Kafka topic so that those message will insert that to DB.

### /employees
This api is get type REST call, you will get the list of employee data with below request parameters

Request
```
page = Page number
limit = Page per page
email = email in string format for email filter
company = company in string format for company filter
```

Response
```
{
    "total": "512",
    "per_page": 10,
    "offset": 0,
    "to": 10,
    "last_page": 52,
    "current_page": 1,
    "from": 0,
    "data": [{
        "name": "KSAXN",
        "phone": "1234567897",
        "email": "ksaxn@lehla.com",
        "company": "LEHLA"
    }]
}
```

## 🚀 Deployment <a name = "deployment"></a>

if you provide the NODE_ENV as development that is will work as express server. Which can be run at EC2 instance.

If you use NODE_ENV other the development than it will work as lambda function.

## ⛏️ Built Using <a name = "built_using"></a>

- [PostgreSQL](https://www.postgresql.org/) - Database
- [Express](https://expressjs.com/) - Server Framework
- [NodeJs](https://nodejs.org/en/) - Server Environment
- [KafkaJs](https://kafka.js.org/) - Kafka JS

## ✍️ Authors <a name = "authors"></a>

- [@hitesh1986in](https://github.com/hitesh1986in)

