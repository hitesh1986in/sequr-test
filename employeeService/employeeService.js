const knex = require('../knex/knex.js');

class EmployeeService {

    static getEmployeeList(req, res) {
        let where = {};
        let page = (req.query.page && parseInt(req.query.page) < 1) ? parseInt(req.query.page) * limit : 1;
        let limit = (req.query.limit) ? req.query.limit : 10;

        if (req.query.email) {
            where.email = req.query.email;
        }
        if (req.query.company) {
            where.company = req.query.company;
        }

        const outputData = {};
        const offset = (page - 1) * limit;

        Promise.all([
            knex.from('employee').count('* as count').first(),
            knex.select('name', 'phone', 'email', 'company')
                .from('employee')
                .where(where)
                .limit(limit)
                .offset(offset)
        ]).then(([total, rows]) => {
            let count = total.count;
            outputData.total = count;
            outputData.per_page = limit;
            outputData.offset = offset;
            outputData.to = offset + rows.length;
            outputData.last_page = Math.ceil(count / limit);
            outputData.current_page = page;
            outputData.from = offset;
            outputData.data = rows;
            res.json(outputData);
        }).catch((error) => {
            console.log('ERROR: While sending the employee list', error);
            res.send(500);
        });
    }

}

module.exports = EmployeeService;