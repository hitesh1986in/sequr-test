
exports.up = function (knex) {
    return knex.schema
        .createTable('employee', function (table) {
            table.increments('id').primary();
            table.string('name', 50).notNullable();
            table.string('phone', 15).notNullable();
            table.string('email', 255).unique().notNullable();
            table.string('company', 255).notNullable();
            table.timestamps(false, true);
        })
};

exports.down = function (knex) {

};
