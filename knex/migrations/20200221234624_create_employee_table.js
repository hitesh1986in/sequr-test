
exports.up = function (knex) {
    return knex.schema
        .createTable('employee', function (table) {
            table.increments('id');
            table.string('name', 50).notNullable();
            table.string('phone', 15).notNullable();
            table.string('email', 255).notNullable().unique();
            table.string('company', 255).notNullable();
        })
};

exports.down = function (knex) {

};
