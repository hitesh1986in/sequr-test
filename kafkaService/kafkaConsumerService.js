const knex = require('../knex/knex.js');
const { Kafka, logLevel } = require('kafkajs');

const kafka = new Kafka({
    logLevel: logLevel.INFO,
    brokers: [process.env.KafkaBroker1],
    clientId: 'sequr-employee-consumer',
})

const topic = process.env.kafkaTopic;
const consumer = kafka.consumer({ groupId: 'sequr-employee-group' });

const run = async () => {
    await consumer.connect();
    await consumer.subscribe({ topic, fromBeginning: true });
    await consumer.run({
        eachMessage: async ({ topic, partition, message }) => {
            const prefix = `${topic}[${partition} | ${message.offset}] / ${message.timestamp}`
            console.log(`- ${prefix} ${message.key}#${message.value}`);
            if (message.key.toString() === 'csv-row') {
                knex('employee').returning('id').insert(JSON.parse(message.value.toString())).then((data) => {
                    console.log(`Message consume for ${message.value} and inserted to db with id ${data}`);
                }).catch((error) => {
                    console.log(`Message consume for ${message.value} however error for inserting it.`);
                });
            }
        },
    });
};

run().catch(e => console.error(`[example/consumer] ${e.message}`, e));

const errorTypes = ['unhandledRejection', 'uncaughtException'];
const signalTraps = ['SIGTERM', 'SIGINT', 'SIGUSR2'];

errorTypes.map(type => {
    process.on(type, async e => {
        try {
            console.log(`process.on ${type}`)
            console.error(e)
            await consumer.disconnect()
            process.exit(0)
        } catch (_) {
            process.exit(1)
        }
    })
});

signalTraps.map(type => {
    process.once(type, async () => {
        try {
            await consumer.disconnect()
        } finally {
            process.kill(process.pid, type)
        }
    })
});
