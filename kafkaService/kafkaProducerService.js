const { Kafka } = require('kafkajs')

const kafka = new Kafka({
    clientId: 'sequr-employee-producer',
    brokers: [process.env.KafkaBroker1]
});

class KafkaService {

    static async putMessage(data) {
        const producer = kafka.producer()

        await producer.connect()
        await producer.send({
            topic: process.env.kafkaTopic,
            messages: [{
                key: process.env.kafkaKey,
                value: JSON.stringify({
                    name: (data[0]) ? data[0] : '',
                    phone: (data[1]) ? data[1] : '',
                    email: (data[2]) ? data[2] : '',
                    company: (data[3]) ? data[3] : '',
                })
            }]
        });

        await producer.disconnect()
    }
}

module.exports = KafkaService;