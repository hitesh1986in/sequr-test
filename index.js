'use strict';

const dotenv = require('dotenv');
const env = process.env.NODE_ENV || 'development';
dotenv.config({ path: env + '.env' });

const http = require('http');
const express = require('express');
const app = express();
const api = require('./routes');

const server = http.createServer(app);
const port = 9000;

app.use('/api', api);

// Default route
app.use((req, res) => {
    res.sendStatus(404);
});
// Start server
function startServer() {
    server.listen(port, function () {
        console.log('Express server listening on ', port);
    });
}

// Initialize kafka consume service
require('./kafkaService/kafkaConsumerService');

if (env === 'development') {
    setImmediate(startServer);
} else {
    const userAwsServerLessExpress = require('aws-serverless-express');
    exports.handler = (event, context) => {
        context.callbackWaitsForEmptyEventLoop = false;
        const server = userAwsServerLessExpress.createServer(userApp);
        userAwsServerLessExpress.proxy(server, event, context);
    };
}

